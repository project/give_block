<?php

namespace Drupal\give_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Give block.
 *
 * @Block(
 *   id = "give_block_block",
 *   admin_label = @Translation("Give block"),
 *   category = @Translation("Give block")
 * )
 */
class GiveBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * Class Resolver service.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new ControllerBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver service.
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ClassResolverInterface $class_resolver,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->classResolver = $class_resolver;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
    ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('class_resolver'),
      $container->get('entity_type.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $give_forms = $this->entityTypeManager->getStorage('give_form')->getQuery()->execute();
    $storage = $this->entityTypeManager->getStorage('give_form');
    $give_forms_data = $storage->loadMultiple($give_forms);

    // If there is no give_form in the configuration,
    // use the default form.
    if (array_key_exists('give_form', $this->configuration)) {
      $default_form = $this->configuration['give_form'];
    }
    else {
      $config = $this->configFactory->get('give.settings');
      $default_form = $config->get('default_form');
    }

    $options = [];
    foreach ($give_forms_data as $form_name => $form_obj) {
      $options[$form_name] = $form_name;
      $options[$form_name] = $form_obj->label();
    }
    $form['give_form'] = [
      '#type' => 'select',
      '#title' => $this->t('Give Form'),
      '#description' => $this->t('Give form to display'),
      '#options' => $options,
      '#default_value' => $default_form,
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['give_form'] = $form_state->getValue('give_form');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $give_page_controller = $this->classResolver->getInstanceFromDefinition('\Drupal\give\Controller\GiveController');

    $storage = $this->entityTypeManager->getStorage('give_form');
    $give_form = $storage->load($this->configuration['give_form']);

    return $give_page_controller->giveSitePage($give_form);
  }

}
