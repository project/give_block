Give Block

Give Block allows you to display a Give 
(https://drupal.org/project/give) form in a block.  The user can choose
from the Give forms configured on the site.
